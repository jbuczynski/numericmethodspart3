/**
 * Created by jakub on 13.01.15.
 */
public class EulerArghoritm {

    private  double maxBlad = -1;

    public  double getMaxBlad() throws Exception {
        if(maxBlad > -1) {
            return maxBlad;
        }
        throw new Exception("Powinienes najpeirw wywolac metode run");

    }

    public Double run() {

         Double xi ;
         Double yi;
         Double dokl;
         Double bladTemp;
         yi = Data.Y_0;
         xi = Data.X_0;
         int i = 0;
        do {


            yi = yi + Data.getStep() * Data.evaluateDerivativeFunction(xi, yi);
            dokl = Data.getAccurateEvaluation(xi);

            bladTemp = Math.abs(yi - dokl);
            System.out.println("Przyblizenie: " + yi + " Rozwiazanie dokladne: " + dokl);
            xi =  xi + Data.getStep();
            if(maxBlad < bladTemp) {
                maxBlad = bladTemp;
            }
        } while(xi <= Data.getA());

        return yi;
    }
}
