import java.util.InputMismatchException;
import java.util.Scanner;

public class UserInterface {


    public static void main(String[] args) {
        //Wczytać naturalną n >= 1 i rzeczywistą a > 1

        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Proszę podać liczbe natiralną n >= 1 ");
            try {
                //int i = 0.5;
                int temp = scanner.nextInt();
                if (temp >= 1) {
                    Data.setN(temp);
                } else {
                    System.out.println("Wartość x0 musi być mniejsz bądź równa 50");
                }
            } catch (InputMismatchException e) {
                System.out.println("Podaj liczbe natiralną!");
            }

        } while (Data.getN() == null);



        do {
            System.out.println("Proszę podać liczbe rzeczywistą a: ");
            try {
                //int i = 0.5;
                double temp = scanner.nextDouble();
                if (temp >= 1) {
                    Data.setA(temp);
                } else {
                    System.out.println("Wartość x0 musi być mniejsz bądź równa 1");
                }
            } catch (InputMismatchException e) {
                System.out.println("Podaj liczbe natiralną!");
            }

        } while (Data.getN() == null);

        EulerArghoritm ea = new EulerArghoritm();
        System.out.println("obliczam rozwiazanie metodą Eulera: " );
        ea.run();

        try {
            System.out.println("Blad maksymalny: " + ea.getMaxBlad());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }



    }


}
