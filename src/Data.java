/**
 * Created by jakub on 13.01.15.
 */
public class Data {

    private static Integer n;
    private static Double a;
    public static final Double Y_0 = 1.0;
    public static final Double X_0 = 1.0;

    public static Integer getN() {
        return n;
    }

    public static void setN(int n) {
        Data.n = n;
    }

    public static Double getA() {
        return a;
    }

    public static void setA(double a) {
        Data.a = a;
    }

    public static Double evaluateDerivativeFunction(Double x, Double y) {
        return Math.sqrt(2*x - y) - 3 * x + 2;
    }

    public static Double getAccurateEvaluation(Double x) {
        return 2 * x - Math.pow(x,2);
    }

    public static Double getStep() {
        return (a-1.0)/n;
    }

    public static Integer[] getPolyNumersAsArray() {
        Integer[] array = new Integer[]{2,-1,-3,2};
        return array;
    }



}
